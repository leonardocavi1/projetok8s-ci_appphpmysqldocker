echo "Criando as imagens..."

docker build -t leonardocavi/projetok8s2-appphp:1.0 app/.
docker build -t leonardocavi/projetok8s2-mysql:1.0 mysql/.

echo "Realizando o push das imagens..."

docker push leonardocavi/projetok8s2-appphp:1.0
docker push leonardocavi/projetok8s2-mysql:1.0

echo "Criando serviços no cluster K8s..."

kubectl.exe apply -f ./secrets.yml
kubectl.exe apply -f ./load-balancer.yml

echo "Criando os deployments..."

kubectl.exe apply -f ./mysql-deployment.yml
kubectl.exe apply -f ./app-deployment.yml

